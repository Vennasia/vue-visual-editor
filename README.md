## 架构
![](readme-assets/00.png)

![](readme-assets/01.png)
+ 描述画布如何渲染

## 关于element-plus
使用vue-cli创建项目后

emmm 莫名其妙运行 `npm install element-plus` 后, 再次启动项目会报以下错误

![](readme-assets/02.png)

并且此时即使运行 `npm uninstall element-plus`, 此问题也无法修复

---

可以使用 `vue add element-plus`, 不会出现上述bug

![](readme-assets/03.png)

## 关于对齐线
![](readme-assets/对齐线01.png)

![](readme-assets/对齐线02.png)

![](readme-assets/对齐线03.png)

![](readme-assets/对齐线04.png)

快速贴边↓
![](readme-assets/对齐线05---快速贴边.png)

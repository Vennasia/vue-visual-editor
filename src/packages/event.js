import mitt from 'mitt';

//https://github.com/developit/mitt
export const events = mitt();

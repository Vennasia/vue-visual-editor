import {defineComponent, computed, inject, onMounted, ref} from 'vue';

export default defineComponent({
  props:{
    block:{
      type: Object
    }
  },
  setup(props){
    const blockStyles = computed(() => ({
      top: `${props.block.top}px`,
      left: `${props.block.left}px`,
      zIndex: `${props.block.zIndex}`
    }));

    const config = inject('config');
    console.log('config', config);

    const blockRef = ref(null);
    onMounted(()=>{
      let {offsetWidth, offsetHeight} = blockRef.value;
      if (props.block.alignCenter) { //说明是拖拽松手的时候才渲染的, 其它默认渲染到页面上的内容不需要居中
        props.block.left = props.block.left - offsetWidth/2;
        props.block.top = props.block.top - offsetHeight/2;
        props.block.alignCenter = false;
        //todo 原则上这里应该emit出去改, 但vue3proxy其实这里已经改了
      }
      props.block.width = offsetWidth;
      props.block.height = offsetHeight;
    })

    return ()=>{
      const component = config.componentMap[props.block.key]
      const RenderComponent = component.render();
      return <div class="editor-block" style={blockStyles.value} ref={blockRef}>
        {RenderComponent}
      </div>;
    }

  }
})

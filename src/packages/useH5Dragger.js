import {events} from "./event";

/**
 * h5拖拽
 * @param containerRef
 * @param dropCB
 * @returns {{dragstart: dragstart, dragend: dragend}}
 */
export function useH5Dragger(containerRef, dropCB){
  let currentComponent = null;

  // dragstart  开始拖动时触发
  // dragenter  进入元素中触发 此时需要添加一个移动标识
  // dragover   在目标元素经过触发 必须要阻止默认行为 否则不能触发 【drop】 事件
  // dragleave  离开元素的时候触发 此时需要增加一个禁用标识
  // drop       松手的时候触发
  const dragenter = (e) => {
    console.log('dragenter ---');
    e.dataTransfer.dropEffect = 'move' //The possible values are "none", "copy", "link", and "move" //它将影响拖动时显示的光标
  };
  const dragover = (e) => {
    console.log('dragover ---');
    e.preventDefault(); //必须有 这样阻止了 下面的drop事件才能触发
  };
  const dragleave = (e) => {
    console.log('dragleave ---');
    e.dataTransfer.dropEffect = 'none';
  };
  const drop = (e) => {
    console.log('drop ---');

    dropCB && typeof dropCB === 'function' && dropCB(e, currentComponent);

    currentComponent = null;
  };

  const dragstart = (e, component) => {
    console.log('dragstart ---');
    console.log('e:', e)
    console.log('component:', component)
    console.log('containerRef.value:', containerRef.nodeType ? containerRef : containerRef.value);

    currentComponent = component;

    const el = containerRef.nodeType ? containerRef : containerRef.value
    el.addEventListener('dragenter', dragenter);
    el.addEventListener('dragover', dragover);
    el.addEventListener('dragleave', dragleave);
    el.addEventListener('drop', drop);

    events.emit('start');
  };
  const dragend = (e) => {
    const el = containerRef.nodeType ? containerRef : containerRef.value
    el.removeEventListener('dragenter', dragenter);
    el.removeEventListener('dragover', dragover);
    el.removeEventListener('dragleave', dragleave);
    el.removeEventListener('drop', drop);

    events.emit('end');
  };

  return {
    dragstart,
    dragend
  }
}

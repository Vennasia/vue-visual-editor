import {reactive} from "vue";
import {events} from "@/packages/event";

/**
 * 集体拖拽
 * @param chosenData
 * @param lastSelectedBlock
 * @param data
 * @returns {{mousedown: mousedown, markLine: UnwrapNestedRefs<{x: null, y: null}>}}
 */
export function useBlockDragger(
  chosenData,          // chosenData 既可以是一组html元素, 也可以是一组vue的响应式数据
  lastSelectedBlock,   // 是vue的computed属性
  data                 // 即data.json对应的响应式对象, 主要用来获取容器的宽高
) {
  let dragState = {
    startX: 0,
    startY: 0,
    dragging: false  // 默认不是正在拖拽
  }

  let markLine = reactive({
    x: null,
    y: null
  })

  const mousemove = (e) => {
    let {clientX: moveX, clientY: moveY} = e;

    if(!dragState.dragging){
      dragState.dragging = true;
      events.emit('start');
    }

    // 用最后一个被点击的元素在之前收集到的所有lines里去匹配
    // 这里的left和top是被拖拽b集群里最后一个被点击的那个元素拖拽开始时的left和top
    let left = moveX - dragState.startX + dragState.startLeft/*b拖拽前的left*/;  //B拖拽后的left
    let top = moveY - dragState.startY + dragState.startTop/*b拖拽前的top*/;     //B拖拽后的top

    //x 是横向对齐(元素处于上下关系, 见 readme-assets/对齐线01.png),
    //y 是纵向对齐(元素处于左右关系, 见 readme-assets/对齐线02.png)
    let y = null;
    for (let i = 0; i < dragState.lines.y.length; ++i) {
      const {top: t, showTop: s} = dragState.lines.y[i];
      // 距离要显示的辅助线5px时显示辅助线
      if (Math.abs(t - top) < 5) {
        y = s; //线要显示的位置

        //实现快速贴边 见 readme-assets/对齐线05---快速贴边.png
        moveY = dragState.startY - dragState.startTop + t;
        break;
      }
    }
    //
    let x = null;
    for (let i = 0; i < dragState.lines.x.length; ++i) {
      const {left: l, showLeft: s} = dragState.lines.x[i];
      // 距离要显示的辅助线5px时显示辅助线
      if (Math.abs(l - left) < 5) {
        x = s; //线要显示的位置

        //实现快速贴边 见 readme-assets/对齐线05---快速贴边.png
        moveX = dragState.startX - dragState.startLeft + l;
        break;
      }
    }
    markLine.x = x;
    markLine.y = y;

    let durX = moveX - dragState.startX;
    let durY = moveY - dragState.startY;

    chosenData.value.chosen.forEach((block, idx) => {
      if (block.nodeType) {
        block.style.left = chosenData.value.chosen[idx].style.left + durX;
        block.style.top = chosenData.value.chosen[idx].style.top + durY;
      } else {
        //因为block也是响应式的,故这里直接修改能刷新视图
        block.left = dragState.startPos[idx].left + durX;
        block.top = dragState.startPos[idx].top + durY;
      }
    });
  }

  const mouseup = (e) => {
    document.removeEventListener('mousemove', mousemove);
    document.removeEventListener('mouseup', mouseup);
    markLine.x = null;
    markLine.y = null;

    if(dragState.dragging) {
      events.emit('end');
      dragState.dragging = false;
    }
  }

  const mousedown = (e) => {
    console.log('lastSelectedBlock.value:',lastSelectedBlock.value);
    //被拖拽的元素叫B
    //相对的, A是被参照物
    const {width:BWidth,height:BHeight,left:BLeft,top:BTop} = lastSelectedBlock.value;

    dragState = {
      dragging: false,
      startX: e.clientX,
      startY: e.clientY,
      startLeft: BLeft, // b拖拽前的left
      startTop: BTop,   // b拖拽前的top
      startPos: chosenData.value.chosen.map(({top, left, nodeType, style}) => {
        if(!nodeType) return ({top, left});

        //emmm ↓这种情况 其实没用上
        return {
          top: style.top,
          left: style.left
        }
      }),

      //辅助线
      lines: (() => {
        // 获取其它没选中的以他们的位置做辅助线
        //chosenData.value.unchosen

        //一个边有5种对齐的可能 上底、上上、中中、下底、下上 (todo 上中 下中 中上 中下)
        //x 是横向对齐(元素处于上下关系, 见 readme-assets/对齐线01.png),
        //y 是纵向对齐(元素处于左右关系, 见 readme-assets/对齐线02.png)
        let lines = {x: [], y: []};

        [
          ...chosenData.value.unchosen,
          // 追加拖拽物料所处的容器作为参照物
          {
            top: 0,
            left: 0,
            width: data.value.container.width,
            height: data.value.container.height
          }
        ].forEach(block => {
          const {top: ATop, left: ALeft, width: AWidth, height: AHeight} = block;

          //showTop: 辅助线的位置
          //top: 元素B对齐辅助线时的top
          //参看 readme-assets/对齐线03.png
          lines.y.push({showTop: ATop, top: ATop});             //对应 对齐线03.png case1
          lines.y.push({showTop: ATop, top: ATop - BHeight});   //对应 对齐线03.png case2
          lines.y.push({showTop: ATop + AHeight/2, top: ATop + AHeight/2 - BHeight/2});   //对应 对齐线03.png case3
          lines.y.push({showTop: ATop+AHeight, top: ATop + AHeight});   //对应 对齐线03.png case4
          lines.y.push({showTop: ATop+AHeight, top: ATop + AHeight - BHeight});   //对应 对齐线03.png case5

          lines.x.push({showLeft: ALeft, left: ALeft});
          lines.x.push({showLeft: ALeft + AWidth, left: ALeft + AWidth});
          lines.x.push({showLeft: ALeft + AWidth / 2, left: ALeft + AWidth / 2 - BWidth / 2});
          lines.x.push({showLeft: ALeft + AWidth, left: ALeft + AWidth - BWidth});
          lines.x.push({showLeft: ALeft, ALeft: ALeft - BWidth});
        });

        console.log('屏幕上所有应该出现辅助线的位置 lines:',lines); //屏幕上所有应该出现辅助线的位置
        return lines
      })()
    }

    document.addEventListener('mousemove', mousemove);
    document.addEventListener('mouseup', mouseup);
  };


  return {
    mousedown,
    markLine
  }
}

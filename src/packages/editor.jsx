import {computed, defineComponent, inject, ref} from 'vue';
import './editor.scss';
import EditorBlock from './editor-block';
import deepcopy from "deepcopy";
import {useH5Dragger} from "./useH5Dragger";
import {useChosen} from "./useChosen";
import {useBlockDragger} from "./useBlockDragger";
import {useCommand} from "@/packages/useCommand";
import {$dialog} from "@/components/Dialog.jsx";
import {$dropdown, DropdownItem} from "@/components/Dropdown.jsx";

export default defineComponent({
  props: {
    modelValue: {type: Object}
  },
  emits: ['update:modelValue'],
  setup(props, ctx) {
    console.log(' --- --- -- setup --- --- --');
    // 预览的时候 内容不能再操作
    // 可以使用组件原本的功能, 比如 点击、输入内容
    const previewRef = ref(false);
    // 是否打开编辑器
    const editorRef = ref(true);

    const data = computed({
      get() {
        return props.modelValue;
      },
      set(newValue) {
        ctx.emit('update:modelValue', deepcopy(newValue));
      }
    });

    const containerStyle = computed(() => ({
      width: data.value.container.width + 'px',
      height: data.value.container.height + 'px'
    }));

    const config = inject('config')

    const containerRef = ref(null);


    const {dragstart,dragend} = useH5Dragger(containerRef,(e, currentComponent)=>{
      console.log('drop ---');
      let blocks = data.value.blocks; //内部已经渲染的组件
      data.value = {
        ...data.value,
        blocks: [
          ...blocks,
          {
            top: e.offsetY,
            left: e.offsetX,
            zIndex: 1,
            key: currentComponent.key,
            alignCenter: true //默认drag drop时的e.offsetX/Y 是鼠标的位置（即放置的组件的左上角对齐我们的鼠标）, 但我们希望放置的组件是居中放置的
          }
        ]
      }
    });


    //该use函数应该放在renderFn里, 因为我们这里传递了 data.value.blocks, 而这个值每次更新页面都是会变化的, 故不能放在renderFn外, 这样它得不到更新 (除非你不传data.value.blocks 而是把整个data传进去, 但这样的话这个钩子就不大解耦了)
    //但这样每次renderFn重新执行 这里都会再执行一遍, 有状态的的hook 比起强行解耦 还是冗余一点好(？)
    //并且我们后面紧跟着调用的 useBlockDragger 返回的 markLine 是一个响应式对象, 就更不能放在renderFn里了, 因为假若放在renderFn里, 那就会每次renderFn重新执行 useBlockDragger就会重新执行, 这样返回的markLine永远是全新的, 永远是markLine处于初始值的情况, 也就会出现我们之前提交运行 markLine 的 x、y 始终赋不上值的问题 (因为一直是最新的)
    //todo So 如果这时候还要考略解耦 可以考虑调用useChosen时传递data取值时的key的名字, useBlockDragger 也是同理
    const {blockMousedown, chosenData, lastSelectBlock, clearBlockChosen} = useChosen(containerRef, data, 'chosen',previewRef,(e)=>{
      mousedown(e);
    });

    //带有状态的use函数一定不要放在renderFn里, 不然每次状态都被初始化, 从实际效果上来讲也就得不到响应
    const {markLine,mousedown} = useBlockDragger(chosenData, lastSelectBlock, data);
    console.log('markLine:',markLine);


    const {commands} = useCommand(data,chosenData);
    const buttons = [
      {label: '撤销', icon: 'icon-back', handler: () => commands.undo()},
      {label: '重做', icon: 'icon-forward', handler: () => commands.redo()},
      {
        label: '导出', icon: 'icon-export', handler: () => {
          $dialog({
            title: '导出json使用',
            content: JSON.stringify(data.value),
          });
        }
      },
      {
        label: '导入', icon: 'icon-import', handler: () => {
          $dialog({
            title: '导入json使用',
            content: '',
            footer: true,
            onConfirm(jsonStr) {
              commands.updateContainer(JSON.parse(jsonStr));
            }
          });
        }
      },
      {label: '置顶', icon: 'icon-place-top', handler: () => commands.placeTop()},
      {label: '置底', icon: 'icon-place-bottom', handler: () => commands.placeBottom()},
      {label: '删除', icon: 'icon-delete', handler: () => commands.delete()},
      {
        label: () => previewRef.value ? '编辑' : '预览',
        icon: () => previewRef.value ? 'icon-edit' : 'icon-browse',
        handler: () => {
          previewRef.value = !previewRef.value;
          clearBlockChosen();
        }
      },
      {
        label: '关闭', icon: 'icon-close', handler: () => {
          editorRef.value = false;
          clearBlockChosen();
        }
      }
    ];

    const onContextMenuBlock = (e,block)=>{
      e.preventDefault();
      $dropdown({
        el: e.target, //以哪个元素为准产生一个dropdown
        content: () => {
          return <>
            <DropdownItem label="删除节点" icon="icon-delete" onClick={() => commands.delete()}/>
            <DropdownItem label="置顶节点" icon="icon-place-top" onClick={() => commands.placeTop()}/>
            <DropdownItem label="置底节点" icon="icon-place-bottom" onClick={() => commands.placeBottom()}/>
            <DropdownItem label="查看数据" icon="icon-browse" onClick={() => {
              $dialog({
                title: '查看节点数据',
                content: JSON.stringify(block)
              });
            }}/>
            <DropdownItem label="导入节点" icon="icon-import" onClick={() => {
              $dialog({
                title: '导入节点数据',
                content: '',
                footer: true,
                onConfirm(content){
                  content = JSON.parse(content);
                  commands.updateBlock(content,block);
                }
              });
            }}/>
          </>
        }
      });
    }

    return () => {
      console.log(' --- --- -- renderFn --- --- --');
      return editorRef.value ?
        <div class='editor'>
          {/*
            注意：onDragstart的start的's'是小写的, 虽然jsx提示是大写的, 但它提示的是react的jsx
          */}
          <div className="editor-left">
            {/* 根据注册列表, 渲染对应的内容 */}
            {config.componentList.map(component => (
              <div
                className="editor-left-item"
                draggable
                onDragstart={e => dragstart(e, component)}
                onDragend={dragend}
              >
                <span>{component.label}</span>
                <div>{component.preview()}</div>
              </div>
            ))}
          </div>
          <div className="editor-top">
            {
              buttons.map((btn, index) => {
                const icon = typeof btn.icon === 'function' ? btn.icon() : btn.icon;
                const label = typeof btn.label === 'function' ? btn.label() : btn.label;

                return <div class='editor-top-button' onClick={btn.handler}>
                  <i class={icon}/>
                  <span>{label}</span>
                </div>
              })
            }
          </div>
          <div className="editor-right">属性控制栏</div>
          <div className="editor-container">
            {/* 负责产生滚动条 */}
            <div className="editor-container-canvas">
              {/* 负责产生内容区域 */}
              <div class="editor-container-canvas__content" style={containerStyle.value}
                   ref={containerRef}>
                {
                  (data.value.blocks.map((block, index) => (
                    <EditorBlock
                      class={block.chosen ? 'editor-block-focus' : ''}
                      class={previewRef.value ? 'editor-block-preview' : ''}
                      block={block}
                      onMousedown={(e) => blockMousedown(e, block, index)}
                      onContextmenu={(e) => onContextMenuBlock(e, block)}
                    />
                  )))
                }

                {markLine.x !== null &&
                <div class='line-x' style={{left: markLine.x + 'px'}}></div>}
                {markLine.y !== null && <div class='line-y' style={{top: markLine.y + 'px'}}></div>}
              </div>
            </div>
          </div>
        </div>
        : <div>
          <div
            className="editor-container-canvas__content"
            style={containerStyle.value}
            style={{margin: 0}}
          >
            {
              (data.value.blocks.map((block, index) => (
                <EditorBlock
                  class='editor-block-preview'
                  block={block}
                />
              )))
            }
          </div>
          <div style={{marginTop: '20px'}}>
            <ElButton type='primary' onClick={() => editorRef.value = true}>继续编辑</ElButton>
          </div>
        </div>;
    }
  }
})

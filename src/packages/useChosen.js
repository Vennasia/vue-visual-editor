import {computed, nextTick, onUnmounted, ref} from "vue";

/**
 * 选中与不选中
 * @param containerRef
 * @param data
 * @param key
 * @param cb
 * @returns {{chosenData: ComputedRef<{chosen: [], unchosen: []}>, blockMousedown: blockMousedown}}
 */
export function useChosen(
  containerRef,             //可以是ref, 也可以直接是作为整个选择区域的html容器元素
  data,                     //data可以是一组作为item的html元素, 也可以是一组vue的响应式对象(对应一个个组件实例(or html元素))
  key = 'selected',  //指定item表示选中状态的那个属性key
  previewRef,               //是否是预览模式
  cb                        //item点击后会触发的回调
){
  console.log('--- --- --- useChosen --- --- ---');
  const isPreview = computed(()=>previewRef && previewRef.value);

  // 我们需要知道最后一个被选中的index是谁(blocks[index]), 对齐线是以最后一个为准的
  // selectIndex 指向我们最新点击的那个的index, 也就是最后一个
  // -1 表示还没有任何一个被选中
  const selectIndex = ref(-1);

  //最后选择的那一个元素
  const lastSelectBlock = computed(()=>data.value.blocks[selectIndex.value])

  const clearBlockChosen = ()=>{
    data.value.blocks.forEach(block => block[key] = false);
    selectIndex.value = -1;
  }
  const containerMousedown = () => {
    if (isPreview.value) return;
    clearBlockChosen();
  };

  const blockMousedown = (e, block, index) => {
    if (isPreview.value) return;
    e.preventDefault();
    e.stopPropagation();

    if(e.ctrlKey){
      block[key] = !block[key];
    }else {
      if(!block[key]){
        clearBlockChosen();
        block[key] = true;
      } else {

        //block[key] = false;
        //↑如果你期望再次点击时不取消选中状态(这种情况一般是点击容器背景时或则按住ctrl键进行点击时才会取消),
        //那么这里应该注释掉
      }
    }

    selectIndex.value = index;
    cb && typeof cb === 'function' && cb(e);
  };

  const chosenData = computed(()=>{
    let chosen = [];
    let unchosen = [];
    data.value.blocks
      .forEach(block => (block[key] ? chosen : unchosen).push(block));

    return {chosen, unchosen};
  })

  nextTick()
    .then(()=>{
      containerRef.nodeType?
        containerRef.addEventListener('mousedown', containerMousedown):
        containerRef.value.addEventListener('mousedown', containerMousedown);
    })

  onUnmounted(()=>{
    containerRef.nodeType ?
      containerRef.removeEventListener('mousedown', containerMousedown) :
      containerRef.value.removeEventListener('mousedown', containerMousedown);
  })

  return {
    blockMousedown,
    chosenData,
    lastSelectBlock,
    clearBlockChosen
  }
}

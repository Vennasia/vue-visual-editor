import {events} from "./event";
import deepcopy from "deepcopy";
import {onUnmounted} from "vue";

export function useCommand(data,chosenData/*置顶置底时有用*/){
  const state = {
    current: -1,      // 前进后退的索引值 -1表示没有记录
    queue:[],         // 存放所有操作命令
    commands:{},      // 制作命令和执行功能的一个映射表 undo:()=>{} redo:()=>{}
    commandArray:[],  // 存放所有的命令
    destroyArray:[],  // init返回的销毁函数的列表
  }

  const registry = (command) => {
    state.commandArray.push(command);
    state.commands[command.name] = (...args) => {
      const {redo,undo} = command.execute(...args);
      redo();

      if(!command.pushQueue) return;

      let {queue, current} = state;
      if (queue.length > 0) {
        //为什么要截取？
        //因为我们可能 a -> b -> c -> d ,
        //然后连续撤销两次, 然后有点击了一个全新的操作
        //就变成了 a -> b -> x
        //故我们需要拿特别记录在外的current指针来截取 (a -> b -> c -> d ------> a -> b)
        queue = queue.slice(0, state.current + 1);

        state.queue = queue;
      }
      state.queue.push({redo, undo});
      state.current = current + 1;
    };
  }

  registry({
    name: 'redo',
    keyboard: 'ctrl+y',
    execute(){
      return {
        redo(){
          console.log('redo')
          let item = state.queue[state.current + 1];
          console.log(item);
          if(item) {
            item.redo && item.redo();
            state.current++;
          }
        }
      }
    }
  });

  registry({
    name: 'undo',
    keyboard: 'ctrl+z',
    execute(){
      return {
        redo(){
          console.log('undo')
          if(state.current === -1) return;
          let item = state.queue[state.current];
          console.log(item);
          if(item) {
            item.undo && item.undo();
            state.current--;
          }
        }
      }
    }
  });

  registry({  // 如果希望将操作放到队列中 可以增加一个属性 标识等会操作要放到队列中
    name: 'drag',
    pushQueue: true,
    init(){  // 初始化操作, 默认就会执行
      console.log('drag command init');
      this.before = null;

      //左侧物料往画布上拖拽开始时会保存一份（即记录当前画布上的物料）
      const start = () => this.before = deepcopy(data.value.blocks);
      //新的物料拖拽到画布上时又会保存一份（即记录当前画布上最新的情况）
      const end = () => state.commands.drag();

      events.on('start',start);
      events.on('end',end);

      return () => {
        events.off('start',start)
        events.off('end',end)
      };
    },
    execute(){
      let before = this.before;
      let after = data.value.blocks;  //现在的状态 被闭包保留, 虽然这里直接取了blocks, 但最好其实还是copy一下 (虽然我们每次更改data.value时其实都会copy一下然后emit上去, 也就说data.value每次都不是同一个引用, 但处于防御性这里其实也还可以copy一下)

      return {
        //前进
        redo(){
          data.value = {...data.value, blocks: after};
        },
        //后退
        undo(){
          data.value = {...data.value, blocks: before};
        }
      }
    }
  });

  registry({
    name: 'updateContainer',  //更新整个容器
    pushQueue: true,
    execute(newValue){ // ← 闭包的用处 就是暂存数据 我们这里保存了redo之前的数据 以便我们后面可能undo时使用
      const state = {
        before: data.value,  //当前的值
        after: newValue      //新值
      };
      return {
        redo:()=>{
          data.value = state.after;
        },
        undo:()=>{
          data.value = state.before;
        }
      }
    }
  })

  registry({
    name: 'placeTop',
    pushQueue: true,
    execute() {
      const before = deepcopy(data.value.blocks);

      // 置顶就是在所有的block中找到最大的
      const after = (() => {
        const {chosen,unchosen} = chosenData.value;
        const maxZIndex = unchosen.reduce((prev,block) => {
          return Math.max(prev, block.zIndex);
        }, -Infinity);
        //让当前选中的比最大的加一
        chosen.forEach(block => block.zIndex = maxZIndex + 1);
        return data.value.blocks;
      })();

      return {
        undo: () => {
          data.value = {...data.value, blocks: before};
          //↑ 这样写 得不到更新, setter的时候 (reactivity/baseHandlers ---> createSetter ---> hasChanged) 会判断新老值是否一样（浅比较）
        },
        redo: () => {
          data.value = {...data.value, blocks: after};
        }
      }
    }
  });

  registry({
    name: 'placeBottom',
    pushQueue: true,
    execute() {
      const before = deepcopy(data.value.blocks);

      // 置顶就是在所有的block中找到最大的
      const after = (() => {
        const {chosen,unchosen} = chosenData.value;
        let minZIndex = unchosen.reduce((prev,block) => {
          return Math.min(prev, block.zIndex);
        }, Infinity) - 1;

        if(minZIndex < 0){
          const dur = Math.abs(minZIndex)
          minZIndex = 0; //让当前的等于0 防止当前的跑到父容器后面去
          unchosen.forEach(block => block.zIndex += dur); //加dur那么大 是防止还有除了当前的 还有层级为负的情况
        }

        chosen.forEach(block => block.zIndex = minZIndex);

        return data.value.blocks;
      })();

      return {
        undo: () => {
          data.value = {...data.value, blocks: before};
          //↑ 这样写 得不到更新, setter的时候 (reactivity/baseHandlers ---> createSetter ---> hasChanged) 会判断新老值是否一样（浅比较）
        },
        redo: () => {
          data.value = {...data.value, blocks: after};
        }
      }
    }
  });

  // 删除选中block
  registry({
    name: 'delete',
    pushQueue: true,
    execute() {
      const state = {
        before: deepcopy(data.value.blocks),
        after: chosenData.value.unchosen
      };
      return {
        redo: () => {
          data.value = {...data.value, blocks: state.after}
        },
        undo: () => {
          data.value = {...data.value, blocks: state.before}
        }
      }
    }
  });

  registry({
    name: 'updateBlock',
    pushQueue: true,
    execute(newBock,oldBock){
      const state = {
        before: deepcopy(data.value.blocks),
        after: (()=>{
          const blocks = [...data.value.blocks];
          const index = data.value.blocks.indexOf(oldBock);
          if(index > -1) {
            blocks.splice(index, 1, newBock);
          }
          return blocks;
        })()
      };
      return {
        redo:()=>{
          data.value = {...data.value,blocks:state.after}
        },
        undo:()=>{
          data.value = {...data.value,blocks:state.before}
        }
      }
    }
  })


  // 实现重做 和 前进的快捷键
  // 是个立即执行函数
  // 这应该不属于命令(不会出现在可点击的命令按钮中) 只是个快捷键
  const keyboardEvent = (() => {
    const keyCodes = {
      90: 'z',
      89: 'y'
    }
    const onKeyDown = (e) => {
      console.log('onKeyDown --- --- -- e:',e);
      const {ctrlKey, keyCode} = e;
      let keyString = [];
      if(ctrlKey) keyString.push('ctrl');
      keyString.push(keyCodes[keyCode]);
      keyString = keyString.join('+');
      state.commandArray.forEach(({keyboard,name})=>{
        if(!keyboard) return;
        if (keyboard === keyString) {
          state.commands[name]();
          e.preventDefault();  //如果浏览器有同名快捷键 则禁用
        }
      })
    };
    const init = () => {  //初始化事件
      console.log('keyboard init --- --- ---');
      window.addEventListener('keydown',onKeyDown)

      return () => {  //销毁事件
        console.log('keyboardEvent destroy --- --- --');
        window.removeEventListener('keydown', onKeyDown);
      };
    };

    return init;
  })();

  (() => {
    state.destroyArray.push(keyboardEvent()); // keyboardEvent已经是返回的init函数了, 这里实际是让返回的init执行了, 返回的是destroy函数 //todo emmm 和正常的命令注册初始化逻辑不在一个地方 有点关注点分离
    state.commandArray.forEach(
      command => command.init  //正常命令初始化在这里初始化 (快捷键是个例外,是在上面一句)
        && state.destroyArray.push(command.init()) //同样, 快捷键的销毁函数导入是在上面一句
    );
  })();

  onUnmounted(() => {// 清理绑定的的事件
    state.destroyArray.forEach(fn => fn && fn());
  });

  return state;
}

import {ElButton, ElInput} from "element-plus";

function createEditorConfig(){
  const componentList = [];
  const componentMap = {};

  return {
    componentList, //用于左侧物料界面渲染
    componentMap,  //用于中央编辑区域渲染
    register:(component)=>{
      componentList.push(component);
      componentMap[component.key] = component;
    }
  }
}

export let registerConfig = createEditorConfig();
console.log('registerConfig:',registerConfig);

registerConfig.register({
  label: '文本',
  preview: () => '预览文本',
  render: () => '渲染文本',
  key: 'text'
})

registerConfig.register({
  label: '文本',
  preview: () => <ElButton>预览按钮</ElButton>,
  render: () => <ElButton>渲染按钮</ElButton>,
  key: 'button'
})

registerConfig.register({
  label: '输入框',
  preview: () => <ElInput placeholder='预览输入框'/>,
  render: () => <ElInput placeholder='渲染输入框'/>,
  key: 'input'
})
